# ddtop

A tool that send to Datadog the data while execute `top`.

[![Gem Version](https://badge.fury.io/rb/ddtop.png)](http://badge.fury.io/rb/ddtop)

## Installation

Add this line to your application's Gemfile:

    gem 'ddtop'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ddtop

## Usage

```sh
$ export DD_API_KEY=...
$ ddtop # The outputted data is transmitted to Datadog.
top - 14:12:24 up  4:23,  4 users,  load average: 2.57, 2.11, 1.43
Tasks: 116 total,   3 running, 113 sleeping,   0 stopped,   0 zombie
Cpu(s): 27.0%us,  6.9%sy,  0.0%ni, 65.9%id,  0.0%wa,  0.0%hi,  0.2%si,  0.0%st
Mem:  15343072k total,  1582800k used, 13760272k free,    19544k buffers
Swap:        0k total,        0k used,        0k free,  1124444k cached

  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND
16901 ec2-user  20   0  125m 6000 1984 R 99.8  0.0   0:03.86 perl
19501 ec2-user  20   0 4871m 232m 4536 S 69.8  1.5   5:48.33 ruby
25238 ec2-user  20   0  107m 2024 1568 S  3.3  0.0   0:13.79 bash
...
```

![datadog-metrics.png](https://bitbucket.org/winebarrel/ddtop/downloads/datadog-metrics.png)

### `--emits` option

Sending all processes data to Datadog is too heavy.

You can specify the process of sending in `--emits` option.

```sh
ddtop --emits httpd.worker,awk,ruby
```

## Contributing

1. Fork it ( http://github.com/<my-github-username>/ddtop/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
