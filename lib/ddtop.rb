module Ddtop
  def self.run
    ::Ddtop::Ddtop.new.run
  end
end

require 'dogapi'
require 'socket'
require 'thread'

require 'ddtop/version'
require 'ddtop/ddtop'
